CREATE DATABASE  IF NOT EXISTS `LarmSoftwareDesign` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `LarmSoftwareDesign`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 192.168.2.195    Database: LarmSoftwareDesign
-- ------------------------------------------------------
-- Server version	5.5.68-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `compra`
--

DROP TABLE IF EXISTS `compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compra` (
  `Numcompra` int(11) NOT NULL AUTO_INCREMENT,
  `Estado` enum('Procesando pago','Pago recibido','Orden en camino','Listo para retirar') DEFAULT NULL,
  `Cantidad` int,
  `Ci` int,
  PRIMARY KEY (`Numcompra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra`
--

LOCK TABLES `compra` WRITE;
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consulta`
--

DROP TABLE IF EXISTS `consulta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consulta` (
  `Ci` int(11) NOT NULL DEFAULT '0',
  `IdProducto` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp,
  PRIMARY KEY (`IdProducto`,`Ci`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consulta`
--

LOCK TABLES `consulta` WRITE;
/*!40000 ALTER TABLE `consulta` DISABLE KEYS */;
/*!40000 ALTER TABLE `consulta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `Rut` int(12) NOT NULL DEFAULT '0',
  `Nomempresa` varchar(30) DEFAULT NULL,
  `Email` varchar(35) DEFAULT NULL,
  `Direccion` varchar(35) DEFAULT NULL,
  `Telefono` int(10) DEFAULT NULL,
  `Contraseña` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encarga`
--

DROP TABLE IF EXISTS `encarga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encarga` (
  `Numcompra` int(11) NOT NULL DEFAULT '0',
  `IdProducto` int(11) NOT NULL DEFAULT '0',
  `Ci` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Numcompra`,`IdProducto`,`Ci`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encarga`
--

LOCK TABLES `encarga` WRITE;
/*!40000 ALTER TABLE `encarga` DISABLE KEYS */;
/*!40000 ALTER TABLE `encarga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `es`
--

DROP TABLE IF EXISTS `es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `es` (
  `Rut` int(12) NOT NULL DEFAULT '0',
  `Ci` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Ci`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `es`
--

LOCK TABLES `es` WRITE;
/*!40000 ALTER TABLE `es` DISABLE KEYS */;
/*!40000 ALTER TABLE `es` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario_pickup`
--

DROP TABLE IF EXISTS `horario_pickup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario_pickup` (
  `IdPickup` int(11) NOT NULL AUTO_INCREMENT,
  `Horario_abre` time DEFAULT NULL,
  `Horario_cierra` time DEFAULT NULL,
  PRIMARY KEY (`IdPickup`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario_pickup`
--

LOCK TABLES `horario_pickup` WRITE;
/*!40000 ALTER TABLE `horario_pickup` DISABLE KEYS */;
/*!40000 ALTER TABLE `horario_pickup` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `pickup`
--

DROP TABLE IF EXISTS `pickup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pickup` (
  `IdPickup` int(11) NOT NULL AUTO_INCREMENT,
  `Direccion` varchar(35) DEFAULT NULL,
  `NomPickup` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`IdPickup`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pickup`
--

LOCK TABLES `pickup` WRITE;
/*!40000 ALTER TABLE `pickup` DISABLE KEYS */;
/*!40000 ALTER TABLE `pickup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `IdProducto` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Producto` varchar(20) DEFAULT NULL,
  `Precio` decimal(15,0) DEFAULT NULL,
  `Condicion` enum('Nuevo','Usado','Restaurado') DEFAULT NULL,
  `Categoria` enum('Falta') DEFAULT NULL,
  `Descripcion` varchar(400) DEFAULT NULL,
  `Nacionalidad` enum('Uruguay','Argentina','Paraguay','Brasil''Chile','Peru','Colombia','Bolivia','Venezuela','Ecuador') DEFAULT NULL,
  PRIMARY KEY (`IdProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pubilca`
--

DROP TABLE IF EXISTS `pubilca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pubilca` (
  `Ci` int(9) NOT NULL DEFAULT '0',
  `IdProducto` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`IdProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pubilca`
--

LOCK TABLES `pubilca` WRITE;
/*!40000 ALTER TABLE `pubilca` DISABLE KEYS */;
/*!40000 ALTER TABLE `pubilca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retira`
--

DROP TABLE IF EXISTS `retira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retira` (
  `IdPickup` int(11) NOT NULL DEFAULT '0',
  `Numcompra` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Numcompra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retira`
--

LOCK TABLES `retira` WRITE;
/*!40000 ALTER TABLE `retira` DISABLE KEYS */;
/*!40000 ALTER TABLE `retira` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiene`
--

DROP TABLE IF EXISTS `tiene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiene` (
  `Rut` int(11) NOT NULL DEFAULT '0',
  `IdPickup` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Rut`,`IdPickup`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiene`
--

LOCK TABLES `tiene` WRITE;
/*!40000 ALTER TABLE `tiene` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiene` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `Ci` int(9) NOT NULL DEFAULT '0',
  `Nombre` varchar(20) DEFAULT NULL,
  `Apellido` varchar(20) DEFAULT NULL,
  `Contraseña` varchar(20) DEFAULT NULL,
  `Fnac` date DEFAULT NULL,
  `Email` varchar(35) DEFAULT NULL,
  `Direccion` varchar(35) DEFAULT NULL,
  `Es` enum('Cliente','Vendedor') DEFAULT NULL,
  PRIMARY KEY (`Ci`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vender`
--

DROP TABLE IF EXISTS `vender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vender` (
  `Ci` int(9) NOT NULL DEFAULT '0',
  `IdProducto` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Ci`,`IdProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vender`
--

LOCK TABLES `vender` WRITE;
/*!40000 ALTER TABLE `vender` DISABLE KEYS */;
/*!40000 ALTER TABLE `vender` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-31 13:01:55
