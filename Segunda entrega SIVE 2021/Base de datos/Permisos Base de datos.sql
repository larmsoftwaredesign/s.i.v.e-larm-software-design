create user cliente;

create user vendedor;

create user administrador;

grant select, INSERT, UPDATE on larmsoftwaredesign.compra to cliente;
grant select on larmsoftwaredesign.compra to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.compra to administrador;


grant select, INSERT on larmsoftwaredesign.consulta to cliente;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.consulta to administrador;

grant select on larmsoftwaredesign.empresa to cliente;
grant select, INSERT, UPDATE on larmsoftwaredesign.empresa to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.empresa to administrador;

grant select, INSERT on larmsoftwaredesign.encarga to cliente;
grant select on larmsoftwaredesign.encarga to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.encarga to administrador; 

grant select on larmsoftwaredesign.es to cliente;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.es to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.es to administrador;

grant select on larmsoftwaredesign.horario_pickup to cliente;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.horario_pickup to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.horario_pickup to administrador;

grant select on larmsoftwaredesign.pickup to cliente;
grant select, INSERT, UPDATE on larmsoftwaredesign.pickup to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.pickup to administrador;

grant select on larmsoftwaredesign.producto to cliente;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.producto to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.producto to administrador;

grant select on larmsoftwaredesign.pubilca to cliente;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.pubilca to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.pubilca to administrador;

grant select, INSERT on larmsoftwaredesign.retira to cliente;
grant select on larmsoftwaredesign.retira to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.retira to administrador;

grant select on larmsoftwaredesign.tiene to cliente;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.tiene to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.tiene to administrador;

grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.usuario to cliente;
grant select on larmsoftwaredesign.usuario to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.usuario to administrador;

grant select on larmsoftwaredesign.vender to cliente;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.vender to vendedor;
grant select, INSERT, UPDATE, DELETE on larmsoftwaredesign.vender to administrador;

SHOW GRANTS FOR <users>;